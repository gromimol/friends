admixAPI.on("load", function (bodycss) {
    var bgName = "background";
    if (admixAPI.name() === bgName) {
        admixAPI.init({
            'resize': [
                {
                    'name': 'state-1',
                    'width': '100%',
                    'height': '100%',
                    'fixed': {
                        'vertical': "center",
                        'horizontal': 'center',
                        'scroll': false // enable/disable image scrolling 
                    },
					css:{
						zIndex: 0
					}
                }
            ]
        });
        document.body.onclick = function () {
            admixAPI.click();
        }
		bodycss.set({
			paddingTop: "0px", // CSS property that sets the top padding of an element
			backgroundColor: "white" // CSS property that sets the background color of an element
		});
    } else {
        admixAPI.panels.add(bgName, {
            body: true,
            first: true,
            visible: true,
            src: admixAPI.src()
        });
    }
});