(function () {
    var siteConfigFunc = document.currentScript.textContent
    var siteConfig = {};
    var bidderSettings = {
        standard: { 
            storageAllowed: true
        }
    };
    var serverConfig ={"rnd":"268435462","auction_id":"3acc4ef1-bf33-4d29-86d3-a4cf5146c06a_29debae5-4372-44a7-8d40-7a65f30c1d4a","prebid_url":"https://cdn.admixer.net/prebidcdn/prebidcdn.js","disable_express":false,"ad_units":[{"bids":[{"bidder":"admixer","analyticsParams":{"adUnitId":1386,"bidId":4499},"params":{"zone":"8a1bca07-7686-43b8-aaa2-ceea683e72f0"}},{"bidder":"admixer","analyticsParams":{"adUnitId":1386,"bidId":4509},"params":{"zone":"93d93ae6-ffc0-43ec-a914-9df6d50d351a"}},{"bidder":"dfp","analyticsParams":{"adUnitId":1386,"bidId":4519},"params":{"id":"delo.ua_300x600_hb_2","section_id":"29636627,22563808739"}},{"bidder":"_mc_1_rtbhouse","analyticsParams":{"adUnitId":1386,"bidId":4540},"params":{"publisherId":"J8Khehe3I13g14SiGHhS","region":"prebid-eu"}},{"bidder":"_mc_1_criteo","analyticsParams":{"adUnitId":1386,"bidId":4541},"params":{"zoneId":"1648603","networkId":"11318"}},{"bidder":"_mc_1_criteo","analyticsParams":{"adUnitId":1386,"bidId":4542},"params":{"zoneId":"1648602","networkId":"11318"}},{"bidder":"_mc_1_criteo","analyticsParams":{"adUnitId":1386,"bidId":4708},"params":{"zoneId":"1648603","networkId":"11318"}},{"bidder":"_mc_1_rtbhouse","analyticsParams":{"adUnitId":1386,"bidId":4760},"params":{"publisherId":"J8Khehe3I13g14SiGHhS","region":"prebid-eu"}},{"bidder":"_mc_1_adf","analyticsParams":{"adUnitId":1386,"bidId":4975},"params":{"mid":"1129652"}},{"bidder":"_mc_1_adtelligent","analyticsParams":{"adUnitId":1386,"bidId":35359},"params":{"aid":"715105"}},{"bidder":"_mc_1_teads","analyticsParams":{"adUnitId":1386,"bidId":40017},"params":{"placementId":"158799","pageId":"144854"}},{"bidder":"_mc_1_teads","analyticsParams":{"adUnitId":1386,"bidId":40232},"params":{"placementId":"158800","pageId":"144855"}},{"bidder":"_mc_1_adf","analyticsParams":{"adUnitId":1386,"bidId":4937},"params":{"mid":"1129654"}},{"bidder":"_mc_1_oftmedia","analyticsParams":{"adUnitId":1386,"bidId":62783},"params":{"placementId":"25141413"}}],"code":"admixer_93d93ae6ffc043eca9149df6d50d351a_zone_41877_sect_822_site_809","mediaTypes":{"banner":{"sizes":[[300,600],[300,250],[300,300]],"playerSize":[300,600]}}}],"prebid_adapters":["admixer","rtbhouse","criteo","adf","adtelligent","teads","oftmedia"],"mc_hooks":[{"prefix":"_mc_1_","multiply":1.0,"adapters":["rtbhouse","criteo","criteo","criteo","rtbhouse","adf","adtelligent","teads","teads","adf","oftmedia"]}],"prebid_modules":["schain","intersectionRtdProvider","mc_hook","admixerIdSystem","adServerDFP","currency","adServer"],"prebid_config":{"realTimeData":{"auctionDelay":100,"dataProviders":[{"name":"intersection","waitForIt":true}]},"adServer":{"dfp":{"express":true}},"priceGranularity":{"buckets":[{"min":0.0,"max":4.0,"increment":0.01}]},"currency":{"adServerCurrency":"USD","granularityMultiplier":1,"rates":{"USD":{"UAH":36.922024000000000000002170147,"USD":1.0,"EUR":1.0042849999999999999999986244}}},"userSync":{"userIds":[{"name":"admixerId","storage":{"name":"admixerId","type":"cookie","expires":30},"params":{"pid":"8c9403df-6eda-4a0a-afb0-be7b68ca005d"}}],"auctionDelay":30},"device":{"ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36","ip":"13.36.47.74"},"s2sConfig":[{"accountId":"8c9403df-6eda-4a0a-afb0-be7b68ca005d","bidders":[],"adapter":"prebidServer","enabled":true,"timeout":500,"endpoint":"https://inv-nets.admixer.net/pbs/openrtb2/auction","extPrebid":{"cache":{"vastxml":{"returnCreative":true}},"targeting":{"pricegranularity":{"ranges":[{"min":0.0,"max":4.0,"increment":0.01}]}}}}],"schain":{
  "validation": "strict",
  "config": {
    "ver": "1.0",
    "complete": 1,
    "nodes": [
      {
        "asi": "admixer.com",
        "sid": "8c9403df-6eda-4a0a-afb0-be7b68ca005d",
        "hp": 1
      }
    ]
  }
}},"external_libs":[{"url":"https://cdn.admixer.net/scripts/load-gpt.js","globalPrefix":"__gptLoaded","siteConfigUrlKey":"dfp_url","path":null}],"prebid_timeout":3000};
    var config = serverConfig;//mergeDeep( siteConfig, serverConfig);
    var AD_UNITS = config.ad_units;
    var PREBID_URL = config.prebid_url;
    var EXTERNAL_LIBS = config.external_libs || [];
    var PREBID_TIMEOUT = config.prebid_timeout;
    var PREBID_MODULES = (config.prebid_modules || []).sort();
    var PREBID_CONFIG = config.prebid_config || {};
    var PREBID_ADAPTERS = (config.prebid_adapters || []).sort();
    var MC_HOOKS = config.mc_hooks || [];
    var AUCTION_ID = config.auction_id || null;
    var PREBID_ANALYTICS = (config.prebid_analytics || []).concat([{
        provider: 'admixer',
        options: {options: true}
    }]);
    var PREBID_ANALYTICS_NAMES = PREBID_ANALYTICS.map(function (a) {
        return a.provider
    }).sort();
    var pbjs = window.pbjs = window.pbjs || {};
    pbjs.que = pbjs.que || [];

    pbjs.bidderSettings = bidderSettings
    
    pbjs.__libLoaded = pbjs.__libLoaded || {
        core: false,
        modules: {},
        aliases: []
    };
    var REQUIRED_MODULES = []
        .concat(PREBID_ADAPTERS.map(function (adapter) {
            return adapter + "BidAdapter"
        }))
        .concat(PREBID_ANALYTICS_NAMES.map(function (analytics) {
            return analytics + "AnalyticsAdapter"
        }))
        .concat(PREBID_MODULES);
    var NEW_MODULES = REQUIRED_MODULES.filter(function (moduleName) {
        return !(moduleName in pbjs.__libLoaded.modules);
    })
        .sort();

    if (NEW_MODULES.length) {
        var dev = "&dev=true";
        // var dev = "";
        var srcQuery = "pm=" + NEW_MODULES.join(",") + (pbjs.__libLoaded.core ? "&no_core=true" : "") + dev + "&rnd=" + serverConfig.rnd;
        var queryHash = (function (str) {
            var hash = 0, i, chr;
            if (str.length === 0) return hash;
            for (i = 0; i < str.length; i++) {
                chr = str.charCodeAt(i);
                hash = ((hash << 5) - hash) + chr;
                hash |= 0; // Convert to 32bit integer
            }
            return hash < 0 ? Math.abs(hash) + "-" : hash;
        })(srcQuery);
        srcQuery += "&hash=" + queryHash;
        var scriptSrc = PREBID_URL + "?" + srcQuery;
        // var scriptSrc = PREBID_URL + "?pm=" + NEW_MODULES.join(",") + (pbjs.__libLoaded.core ? "&no_core=true" : "") + dev + "&rnd=" + serverConfig.rnd;
        NEW_MODULES.forEach(function (nm) {
            pbjs.__libLoaded.modules[nm] = "pending"
        });
        var script = document.createElement('script');
        if (dev) {
            script.crossOrigin = "anonymous"
        }
        script.addEventListener("load", function () {
            NEW_MODULES.forEach(function (nm) {
                pbjs.__libLoaded.modules[nm] = "ready"
            });
        });
        if (pbjs.__libLoaded.core) {
            pbjs.que.push(function () {
                script.src = scriptSrc;
                document.head.appendChild(script);
            });
        } else {
            script.src = scriptSrc;
            document.head.appendChild(script);
        }
        pbjs.__libLoaded.core = true;
        // pbjs.__libLoaded.core = true;
        // script.addEventListener("load", function () {
        //     NEW_MODULES.forEach(function (nm) {
        //         pbjs.__libLoaded.modules[nm] = "ready"
        //     });
        // });
        // script.src = scriptSrc;
        // document.head.appendChild(script);
    }
    PREBID_ANALYTICS = PREBID_ANALYTICS.filter(function (analytic) {
        return (analytic.provider + "AnalyticsAdapter") in pbjs.__libLoaded.modules;
    });
    if (REQUIRED_MODULES.length) {
        var loadInterval = setInterval(function () {
            var pending = REQUIRED_MODULES.find(function (nm) {
                return !(nm in pbjs.__libLoaded.modules) || pbjs.__libLoaded.modules[nm] !== "ready";
            });
            if (!pending) {
                clearInterval(loadInterval);
                onPbLoad();
            }
        }, 16);
    } else {
        onPbLoad();
    }
    EXTERNAL_LIBS.forEach((lib) => {
        if (!pbjs[lib.globalPrefix]) {
            var src = (siteConfig[lib.siteConfigUrlKey] || lib.url) + (lib.path || "");
            var script = document.createElement('script');
            script.src = src;
            document.head.appendChild(script);
            pbjs[lib.globalPrefix] = true;
        }
    })

    function onPbLoad() {
        pbjs.que.push(function () {
            mcHookFlatten(MC_HOOKS).forEach(mc_hook => {
                if (pbjs.__libLoaded.aliases.indexOf(mc_hook.alias) === -1) {
                    // if (mc_hook.alias.indexOf("adform") > -1) {
                    //     // debugger;
                    // }
                    pbjs.aliasBidder(mc_hook.adapter, mc_hook.alias);
                    pbjs.addMCHook({prefix: mc_hook.prefix, multiply: mc_hook.multiply});
                    pbjs.__libLoaded.aliases.push(mc_hook.alias);
                }
            });
            PREBID_ANALYTICS.forEach(pb_an => {
                pbjs.enableAnalytics(pb_an);
            });
            if (siteConfigFunc) {
                (new Function(siteConfigFunc))();
            }
            pbjs.patchConfig(Object.assign({}, PREBID_CONFIG));
            pbjs.initAdServer({
                timeout: PREBID_TIMEOUT,
                adUnits: AD_UNITS,
                auctionId: AUCTION_ID,
            });
        });
    }

    function mcHookFlatten(mcHook) {
        var result = [];
        MC_HOOKS.forEach(mc_hook => {
            mc_hook.adapters.forEach(adapter => {
                result.push({
                    prefix: mc_hook.prefix,
                    adapter: adapter,
                    alias: `${mc_hook.prefix}${adapter}`,
                    multiply: mc_hook.multiply
                })
            })
        });
        return result;
    }
})()