(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"body_atlas_1", frames: [[182,318,207,104],[0,444,380,45],[0,0,512,240],[0,242,500,74],[0,318,180,124]]}
];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.ignorePause = false;
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.delo = function() {
	this.initialize(ss["body_atlas_1"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.nas_pidtrimali = function() {
	this.initialize(ss["body_atlas_1"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.prapor = function() {
	this.initialize(ss["body_atlas_1"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.techiia_logo = function() {
	this.initialize(ss["body_atlas_1"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.top_sto = function() {
	this.initialize(ss["body_atlas_1"]);
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.Tween29 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.nas_pidtrimali();
	this.instance.setTransform(-102,-12.05,0.5368,0.5366);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-102,-12,204,24.1);


(lib.Tween28 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.nas_pidtrimali();
	this.instance.setTransform(-102,-12.05,0.5368,0.5366);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-102,-12,204,24.1);


(lib.Tween27 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AggBcQgOgGgJgNQgIgNAAgVIAAhNQAAgVAIgNQAJgNAOgGQAOgGASAAQATAAAOAGQAOAGAJANQAIANAAAVIAAAqIhVAAIAAAoQAAALAGAFQAGAFAJAAQAKAAAGgFQAGgGAAgKIAAgKIAqAAIAAAFQAAAVgIANQgJANgOAGQgOAGgTAAQgSAAgOgGgAgPg6QgGAEAAAKIAAASIArAAIAAgSQAAgKgGgEQgGgGgKAAQgJAAgGAGg");
	this.shape.setTransform(52.475,3.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKBvQgLgJAAgSIAAh3IgRAAIAAghIARAAIAAgyIApAAIAAAyIATAAIAAAhIgTAAIAABmQAAALADAEQAEAEAMAAIAAAgIgJABIgLAAQgTAAgKgIg");
	this.shape_1.setTransform(34.925,1.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag1BWQgNgNgBgbQAAgQAGgLQAEgLALgIQALgIAPgGIATgIQAIgFAGgGQAFgGABgHQAAgJgGgDQgFgFgJAAQgJABgGAFQgGAGAAAMIAAAEIgpAAIAAgJQAAgPAIgMQAHgMAPgHQAOgHASAAQARAAAOAFQAPAFAJALQAIAKABARIAAByQgBAJACAHQABAGACAFIgrAAIgDgIIAAgJQgHALgJAFQgHAHgRAAQgVAAgNgMgAgIAPQgIAEgEAIQgDAIABAGQgBALAEAHQAFAHAKABQAJgBAHgFQAGgHABgMIAAgsg");
	this.shape_2.setTransform(17.65,3.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAUBfIAAiEQAAgLgFgGQgGgGgJAAQgJAAgFAGQgFAGAAALIAACEIgqAAIAAi4IAqAAIAAAPQAFgHAIgHQAJgGANAAQANAAAKAFQALAFAFAMQAHAMAAASIAACJg");
	this.shape_3.setTransform(-3.55,3.475);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmBZQgPgIgGgNQgGgOAAgRIAAhKQAAgQAGgNQAGgOAPgJQAOgHAYgBQAYABAPAHQAOAJAHAOQAGANAAAQIAABKQAAARgGAOQgHANgOAIQgPAJgYAAQgYAAgOgJgAgPg7QgGAEAAANIAABVQAAAMAGAFQAGAFAJAAQALAAAFgEQAGgFAAgNIAAhVQAAgMgGgGQgGgEgKAAQgJAAgGAFg");
	this.shape_4.setTransform(-24.5268,3.75);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgmB4QgKgGgIgLQgGgMAAgSIAAhaQAAgTAGgMQAIgLAKgGQAMgEANAAQAMAAAIAFQAKAGAEAHIAAhJIAqAAIAAD0IgqAAIAAgTQgFALgKAHQgJAGgKABQgNAAgMgGgAgMgeQgHAGAAALIAABTQAAALAHAGQAGAFAHABQAJgBAFgFQAGgGAAgLIAAhUQAAgKgGgGQgFgFgJAAQgHAAgGAFg");
	this.shape_5.setTransform(-45.65,1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#050581","#0000BE"],[0,1],-135,0,135,0).s().p("AxOD2QhnAAhIhIQhIhIAAhmQAAhlBIhJQBIhIBnAAMAieAAAQBlAABIBIQBJBJAABlQAABmhJBIQhIBIhlAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-135,-24.6,270,49.3);


(lib.Tween26 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AggBcQgOgGgJgNQgIgNAAgVIAAhNQAAgVAIgNQAJgNAOgGQAOgGASAAQATAAAOAGQAOAGAJANQAIANAAAVIAAAqIhVAAIAAAoQAAALAGAFQAGAFAJAAQAKAAAGgFQAGgGAAgKIAAgKIAqAAIAAAFQAAAVgIANQgJANgOAGQgOAGgTAAQgSAAgOgGgAgPg6QgGAEAAAKIAAASIArAAIAAgSQAAgKgGgEQgGgGgKAAQgJAAgGAGg");
	this.shape.setTransform(52.475,3.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKBvQgLgJAAgSIAAh3IgRAAIAAghIARAAIAAgyIApAAIAAAyIATAAIAAAhIgTAAIAABmQAAALADAEQAEAEAMAAIAAAgIgJABIgLAAQgTAAgKgIg");
	this.shape_1.setTransform(34.925,1.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag1BWQgNgNgBgbQAAgQAGgLQAEgLALgIQALgIAPgGIATgIQAIgFAGgGQAFgGABgHQAAgJgGgDQgFgFgJAAQgJABgGAFQgGAGAAAMIAAAEIgpAAIAAgJQAAgPAIgMQAHgMAPgHQAOgHASAAQARAAAOAFQAPAFAJALQAIAKABARIAAByQgBAJACAHQABAGACAFIgrAAIgDgIIAAgJQgHALgJAFQgHAHgRAAQgVAAgNgMgAgIAPQgIAEgEAIQgDAIABAGQgBALAEAHQAFAHAKABQAJgBAHgFQAGgHABgMIAAgsg");
	this.shape_2.setTransform(17.65,3.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAUBfIAAiEQAAgLgFgGQgGgGgJAAQgJAAgFAGQgFAGAAALIAACEIgqAAIAAi4IAqAAIAAAPQAFgHAIgHQAJgGANAAQANAAAKAFQALAFAFAMQAHAMAAASIAACJg");
	this.shape_3.setTransform(-3.55,3.475);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmBZQgPgIgGgNQgGgOAAgRIAAhKQAAgQAGgNQAGgOAPgJQAOgHAYgBQAYABAPAHQAOAJAHAOQAGANAAAQIAABKQAAARgGAOQgHANgOAIQgPAJgYAAQgYAAgOgJgAgPg7QgGAEAAANIAABVQAAAMAGAFQAGAFAJAAQALAAAFgEQAGgFAAgNIAAhVQAAgMgGgGQgGgEgKAAQgJAAgGAFg");
	this.shape_4.setTransform(-24.5268,3.75);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgmB4QgKgGgIgLQgGgMAAgSIAAhaQAAgTAGgMQAIgLAKgGQAMgEANAAQAMAAAIAFQAKAGAEAHIAAhJIAqAAIAAD0IgqAAIAAgTQgFALgKAHQgJAGgKABQgNAAgMgGgAgMgeQgHAGAAALIAABTQAAALAHAGQAGAFAHABQAJgBAFgFQAGgGAAgLIAAhUQAAgKgGgGQgFgFgJAAQgHAAgGAFg");
	this.shape_5.setTransform(-45.65,1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#050581","#0000BE"],[0,1],-135,0,135,0).s().p("AxOD2QhnAAhIhIQhIhIAAhmQAAhlBIhJQBIhIBnAAMAieAAAQBlAABIBIQBJBJAABlQAABmhJBIQhIBIhlAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-135,-24.6,270,49.3);


(lib.Tween25 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.delo();
	this.instance.setTransform(-55.7,-28,0.5384,0.5385);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55.7,-28,111.5,56);


(lib.Tween24 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.delo();
	this.instance.setTransform(-55.7,-28,0.5384,0.5385);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55.7,-28,111.5,56);


(lib.Tween23 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape.setTransform(132.725,16.775);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgNBIIAAh5IgiAAIAAgWIBfAAIAAAWIgiAAIAAB5g");
	this.shape_1.setTransform(124.225,16.775);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgbAAIAAiPIAbAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_2.setTransform(112.55,16.775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_3.setTransform(100.425,16.825);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgsBIIAAiPIAzAAQALAAAJAGQAIAFAFAKQAFAJAAAMQAAAMgFAKQgEAJgKAGQgIAFgMAAIgXAAIAAA7gAgRgHIAOAAQAJAAAGgGQAFgFAAgLQAAgKgFgFQgGgGgJAAIgOAAg");
	this.shape_4.setTransform(88.45,16.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgNBIIAAgSIgIAAQgMABgKgGQgLgGgHgMQgHgMAAgTQAAgSAHgNQAHgMALgGQAKgGAMAAIAIAAIAAgQIAbAAIAAAQIAIAAQALAAAMAGQAKAGAHAMQAHANAAASQAAATgHAMQgHAMgKAGQgLAGgMgBIgIAAIAAASgAAOAhIAGAAQAKAAAGgJQAFgIAAgQQAAgPgFgJQgGgJgKAAIgGAAgAgigYQgGAJAAAPQAAAQAGAIQAFAJAKAAIAGAAIAAhCIgGAAQgKAAgFAJg");
	this.shape_5.setTransform(74.35,16.775);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgYBJIgIgBIAAgWIADACIAFAAQAEAAADgDQADgCACgHIgohxIAfAAIAVBLIAXhLIAeAAIgoB0QgFARgHAHQgGAHgKAAIgJgBg");
	this.shape_6.setTransform(54.625,16.95);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAlBIIAAiCIgbCCIgTAAIgbiBIAACBIgaAAIAAiPIAsAAIASBdIAThdIAsAAIAACPg");
	this.shape_7.setTransform(40.9,16.775);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_8.setTransform(27.025,16.825);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_9.setTransform(14.9,16.775);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAUBcIAAg3IABgcQABgLACgKIAAAAIgoBoIgdAAIAAiQIAaAAIAAA2IAAASIgCASIgCAOIAAAAIAohoIAdAAIAACQgAgShBQgGgEgDgHQgBgHAAgIIANAAQAAAIAFAEQADAFAHAAQAGAAADgFQAEgEABgIIANAAQAAAIgBAHQgEAHgFAEQgHAEgKAAQgMAAgGgEg");
	this.shape_10.setTransform(2.75,14.825);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape_11.setTransform(-6.225,16.775);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAYBXIAAgdIhJAAIAAiQIAcAAIAAB6IAjAAIAAh6IAbAAIAAB6IAJAAIAAAzg");
	this.shape_12.setTransform(-15.15,18.225);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAXBIIgHglIgeAAIgIAlIgeAAIAliPIAkAAIAgCPgAANANIgLhCIgMBCIAXAAg");
	this.shape_13.setTransform(-27.575,16.775);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAlBIIAAiCIgbCCIgTAAIgciBIAACBIgZAAIAAiPIAsAAIASBdIAThdIAsAAIAACPg");
	this.shape_14.setTransform(-41.25,16.775);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgsBIIAAiPIAzAAQAMAAAIAGQAJAFAEAKQAEAJABAMQgBAMgEAKQgEAJgJAGQgJAFgLAAIgYAAIAAA7gAgRgHIAOAAQAKAAAEgGQAGgFAAgLQAAgKgGgFQgEgGgKAAIgOAAg");
	this.shape_15.setTransform(-54.95,16.775);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_16.setTransform(-67.025,16.825);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgMBIIAAgSIgIAAQgNABgLgGQgKgGgHgMQgHgMAAgTQAAgSAHgNQAHgMALgGQALgGAMAAIAIAAIAAgQIAaAAIAAAQIAIAAQALAAALAGQALAGAHAMQAHANAAASQAAATgHAMQgHAMgLAGQgKAGgMgBIgIAAIAAASgAAOAhIAGAAQALAAAEgJQAGgIAAgQQAAgPgGgJQgFgJgKAAIgGAAgAgjgYQgFAJAAAPQAAAQAFAIQAGAJAKAAIAHAAIAAhCIgHAAQgKAAgGAJg");
	this.shape_17.setTransform(-81.2,16.775);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_18.setTransform(-95.25,16.775);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape_19.setTransform(-104.175,16.775);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAXBIIgHglIgeAAIgIAlIgeAAIAliPIAkAAIAgCPgAANANIgLhCIgMBCIAXAAg");
	this.shape_20.setTransform(-118.775,16.775);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_21.setTransform(-130.7,16.775);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AggBjQgQgHgIgOQgJgPAAgaIAAhIQAAgaAJgPQAIgPAQgHQAOgHATAAQATAAAOAHQAPAGAIAQQAJAPAAAaIAABIQAAAagJAPQgIAOgPAHQgOAGgTABQgTgBgOgGgAgThAQgGAIAAARIAABRQAAAQAGAIQAHAJANgBQAMAAAHgHQAGgJAAgQIAAhRQAAgQgGgJQgHgKgMABQgNgBgHAKg");
	this.shape_22.setTransform(-34.55,-11.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AA0BlIAAi2IgmC2IgbAAIgmi2IAAC2IgkAAIAAjJIA9AAIAaCCIAbiCIA9AAIAADJg");
	this.shape_23.setTransform(-53.575,-11.15);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgtBZQgQgRAAgjIAAhVQAAgQAHgNQAHgNAOgHQANgIAUAAQAVAAANAIQAOAHAHAMQAHANAAARIAAAJIgmAAQAAgRgFgIQgFgKgOABQgMgBgFAKQgFAIABARIAAAQIAfAAIAAAeIgfAAIAAAkQAAAOAFAJQAGAJAKgBQAOAAAFgHQAFgJAAgPIAAgOIAmAAIAAALQAAAjgPAPQgPAQggAAQgdAAgQgRg");
	this.shape_24.setTransform(-72.225,-11.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AACBjQgNgHgJgOQgJgPAAgaIAAgeIgiAAIAABdIgnAAIAAjIIAnAAIAABNIAiAAIAAgOQAAgZAJgPQAJgPANgGQAPgHAUAAQATAAAPAHQAPAGAJAPQAIAPAAAZIAABJQAAAagIAPQgJAPgPAHQgPAGgTABQgUgBgPgGgAARhBQgIAJAAAQIAABQQAAAQAIAJQAGAKAOgBQANABAGgKQAIgJAAgQIAAhQQAAgQgIgJQgGgIgNAAQgOAAgGAIg");
	this.shape_25.setTransform(-92.2,-11.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AggBjQgQgHgIgOQgIgPgBgaIAAhIQABgaAIgPQAIgPAQgHQAOgHASAAQAUAAAOAHQAPAGAIAQQAJAPAAAaIAABIQAAAagJAPQgIAOgPAHQgOAGgUABQgSgBgOgGgAgThAQgGAIAAARIAABRQAAAQAGAIQAHAJAMgBQANAAAGgHQAHgJAAgQIAAhRQAAgQgHgJQgGgKgNABQgMgBgHAKg");
	this.shape_26.setTransform(-112.7,-11.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("Ag9BlIAAjJIBGAAQASAAALAHQALAGAFAMQAFALAAANQAAAKgDAJQgEAKgGAHQgGAGgIADQAPAFAHALQAIALAAAUQAAASgFANQgFAOgLAHQgKAIgPAAgAgWBIIAXAAQALABAFgIQAGgIAAgRQAAgOgGgIQgFgIgLAAIgXAAgAgWgSIAUAAQALgBAGgGQAGgIAAgNQAAgNgGgGQgFgHgMABIgUAAg");
	this.shape_27.setTransform(-128.675,-11.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-138.2,-29.8,276.5,59.6);


(lib.Tween22 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape.setTransform(132.725,16.775);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgNBIIAAh5IgiAAIAAgWIBfAAIAAAWIgiAAIAAB5g");
	this.shape_1.setTransform(124.225,16.775);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgbAAIAAiPIAbAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_2.setTransform(112.55,16.775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_3.setTransform(100.425,16.825);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgsBIIAAiPIAzAAQALAAAJAGQAIAFAFAKQAFAJAAAMQAAAMgFAKQgEAJgKAGQgIAFgMAAIgXAAIAAA7gAgRgHIAOAAQAJAAAGgGQAFgFAAgLQAAgKgFgFQgGgGgJAAIgOAAg");
	this.shape_4.setTransform(88.45,16.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgNBIIAAgSIgIAAQgMABgKgGQgLgGgHgMQgHgMAAgTQAAgSAHgNQAHgMALgGQAKgGAMAAIAIAAIAAgQIAbAAIAAAQIAIAAQALAAAMAGQAKAGAHAMQAHANAAASQAAATgHAMQgHAMgKAGQgLAGgMgBIgIAAIAAASgAAOAhIAGAAQAKAAAGgJQAFgIAAgQQAAgPgFgJQgGgJgKAAIgGAAgAgigYQgGAJAAAPQAAAQAGAIQAFAJAKAAIAGAAIAAhCIgGAAQgKAAgFAJg");
	this.shape_5.setTransform(74.35,16.775);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgYBJIgIgBIAAgWIADACIAFAAQAEAAADgDQADgCACgHIgohxIAfAAIAVBLIAXhLIAeAAIgoB0QgFARgHAHQgGAHgKAAIgJgBg");
	this.shape_6.setTransform(54.625,16.95);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAlBIIAAiCIgbCCIgTAAIgbiBIAACBIgaAAIAAiPIAsAAIASBdIAThdIAsAAIAACPg");
	this.shape_7.setTransform(40.9,16.775);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_8.setTransform(27.025,16.825);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_9.setTransform(14.9,16.775);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAUBcIAAg3IABgcQABgLACgKIAAAAIgoBoIgdAAIAAiQIAaAAIAAA2IAAASIgCASIgCAOIAAAAIAohoIAdAAIAACQgAgShBQgGgEgDgHQgBgHAAgIIANAAQAAAIAFAEQADAFAHAAQAGAAADgFQAEgEABgIIANAAQAAAIgBAHQgEAHgFAEQgHAEgKAAQgMAAgGgEg");
	this.shape_10.setTransform(2.75,14.825);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape_11.setTransform(-6.225,16.775);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAYBXIAAgdIhJAAIAAiQIAcAAIAAB6IAjAAIAAh6IAbAAIAAB6IAJAAIAAAzg");
	this.shape_12.setTransform(-15.15,18.225);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAXBIIgHglIgeAAIgIAlIgeAAIAliPIAkAAIAgCPgAANANIgLhCIgMBCIAXAAg");
	this.shape_13.setTransform(-27.575,16.775);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAlBIIAAiCIgbCCIgTAAIgciBIAACBIgZAAIAAiPIAsAAIASBdIAThdIAsAAIAACPg");
	this.shape_14.setTransform(-41.25,16.775);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgsBIIAAiPIAzAAQAMAAAIAGQAJAFAEAKQAEAJABAMQgBAMgEAKQgEAJgJAGQgJAFgLAAIgYAAIAAA7gAgRgHIAOAAQAKAAAEgGQAGgFAAgLQAAgKgGgFQgEgGgKAAIgOAAg");
	this.shape_15.setTransform(-54.95,16.775);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_16.setTransform(-67.025,16.825);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgMBIIAAgSIgIAAQgNABgLgGQgKgGgHgMQgHgMAAgTQAAgSAHgNQAHgMALgGQALgGAMAAIAIAAIAAgQIAaAAIAAAQIAIAAQALAAALAGQALAGAHAMQAHANAAASQAAATgHAMQgHAMgLAGQgKAGgMgBIgIAAIAAASgAAOAhIAGAAQALAAAEgJQAGgIAAgQQAAgPgGgJQgFgJgKAAIgGAAgAgjgYQgFAJAAAPQAAAQAFAIQAGAJAKAAIAHAAIAAhCIgHAAQgKAAgGAJg");
	this.shape_17.setTransform(-81.2,16.775);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_18.setTransform(-95.25,16.775);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape_19.setTransform(-104.175,16.775);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAXBIIgHglIgeAAIgIAlIgeAAIAliPIAkAAIAgCPgAANANIgLhCIgMBCIAXAAg");
	this.shape_20.setTransform(-118.775,16.775);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_21.setTransform(-130.7,16.775);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AggBjQgQgHgIgOQgJgPAAgaIAAhIQAAgaAJgPQAIgPAQgHQAOgHATAAQATAAAOAHQAPAGAIAQQAJAPAAAaIAABIQAAAagJAPQgIAOgPAHQgOAGgTABQgTgBgOgGgAgThAQgGAIAAARIAABRQAAAQAGAIQAHAJANgBQAMAAAHgHQAGgJAAgQIAAhRQAAgQgGgJQgHgKgMABQgNgBgHAKg");
	this.shape_22.setTransform(-34.55,-11.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AA0BlIAAi2IgmC2IgbAAIgmi2IAAC2IgkAAIAAjJIA9AAIAaCCIAbiCIA9AAIAADJg");
	this.shape_23.setTransform(-53.575,-11.15);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgtBZQgQgRAAgjIAAhVQAAgQAHgNQAHgNAOgHQANgIAUAAQAVAAANAIQAOAHAHAMQAHANAAARIAAAJIgmAAQAAgRgFgIQgFgKgOABQgMgBgFAKQgFAIABARIAAAQIAfAAIAAAeIgfAAIAAAkQAAAOAFAJQAGAJAKgBQAOAAAFgHQAFgJAAgPIAAgOIAmAAIAAALQAAAjgPAPQgPAQggAAQgdAAgQgRg");
	this.shape_24.setTransform(-72.225,-11.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AACBjQgNgHgJgOQgJgPAAgaIAAgeIgiAAIAABdIgnAAIAAjIIAnAAIAABNIAiAAIAAgOQAAgZAJgPQAJgPANgGQAPgHAUAAQATAAAPAHQAPAGAJAPQAIAPAAAZIAABJQAAAagIAPQgJAPgPAHQgPAGgTABQgUgBgPgGgAARhBQgIAJAAAQIAABQQAAAQAIAJQAGAKAOgBQANABAGgKQAIgJAAgQIAAhQQAAgQgIgJQgGgIgNAAQgOAAgGAIg");
	this.shape_25.setTransform(-92.2,-11.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AggBjQgQgHgIgOQgIgPgBgaIAAhIQABgaAIgPQAIgPAQgHQAOgHASAAQAUAAAOAHQAPAGAIAQQAJAPAAAaIAABIQAAAagJAPQgIAOgPAHQgOAGgUABQgSgBgOgGgAgThAQgGAIAAARIAABRQAAAQAGAIQAHAJAMgBQANAAAGgHQAHgJAAgQIAAhRQAAgQgHgJQgGgKgNABQgMgBgHAKg");
	this.shape_26.setTransform(-112.7,-11.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("Ag9BlIAAjJIBGAAQASAAALAHQALAGAFAMQAFALAAANQAAAKgDAJQgEAKgGAHQgGAGgIADQAPAFAHALQAIALAAAUQAAASgFANQgFAOgLAHQgKAIgPAAgAgWBIIAXAAQALABAFgIQAGgIAAgRQAAgOgGgIQgFgIgLAAIgXAAgAgWgSIAUAAQALgBAGgGQAGgIAAgNQAAgNgGgGQgFgHgMABIgUAAg");
	this.shape_27.setTransform(-128.675,-11.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-138.2,-29.8,276.5,59.6);


(lib.Tween21 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAlClIAAhiIABg0QABgUAEgRIhIC7Ig1AAIAAkDIAuAAIAABhIAAAhIgCAgIgEAaIBIi8IA1AAIAAEDgAghh2QgLgIgEgMQgEgMAAgOIAZAAQAAAOAHAIQAHAHAMAAQALAAAHgHQAHgIAAgOIAaAAQAAAOgEAMQgFAMgLAIQgMAIgTAAQgVAAgLgIg");
	this.shape.setTransform(80.875,-2.725);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AApCCIgMhEIg3AAIgPBEIg1AAIBCkDIBBAAIA6EDgAAXAYIgUh4IgWB4IAqAAg");
	this.shape_1.setTransform(60.075,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ABECCIAAjqIgBAAIgyDqIgiAAIgxjqIAAAAIAADqIguAAIAAkDIBOAAIAiCoIAiioIBQAAIAAEDg");
	this.shape_2.setTransform(36.25,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAlCCIAAhiIABgzQABgVAEgRIhIC7Ig1AAIAAkDIAuAAIAABhIAAAgIgCAgIgEAbIBIi8IA1AAIAAEDg");
	this.shape_3.setTransform(12.025,0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AhQCCIAAkDIBdAAQAVAAAPAKQAPAKAIASQAJARAAAVQAAAWgJASQgIAQgQALQgPAKgVAAIgqAAIAABqgAgegNIAYAAQASgBAJgJQAJgKAAgUQAAgSgJgKQgJgJgSgBIgYAAg");
	this.shape_4.setTransform(-8.775,0.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgYCCIAAjcIg9AAIAAgnICrAAIAAAnIg9AAIAADcg");
	this.shape_5.setTransform(-28.875,0.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AA2CcIAAg0IhrAAIAAA0IgvAAIAAhcIAPAAQAHgOAEgOQAFgNACgQQADgOACgVIABguIAAhRICTAAIAADbIAPAAIAABcgAgNg4QAAAdgCAWQgDAUgFARQgFARgIAPIBIAAIAAi0IgxAAg");
	this.shape_6.setTransform(-50.325,3.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgYCCIAAkDIAxAAIAAEDg");
	this.shape_7.setTransform(-66.825,0.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAhCCIAAjcIhAAAIAADcIgyAAIAAkDICjAAIAAEDg");
	this.shape_8.setTransform(-82.1,0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-94.1,-22.6,188.3,45.2);


(lib.Tween20 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAlClIAAhiIABg0QABgUAEgRIhIC7Ig1AAIAAkDIAuAAIAABhIAAAhIgCAgIgEAaIBIi8IA1AAIAAEDgAghh2QgLgIgEgMQgEgMAAgOIAZAAQAAAOAHAIQAHAHAMAAQALAAAHgHQAHgIAAgOIAaAAQAAAOgEAMQgFAMgLAIQgMAIgTAAQgVAAgLgIg");
	this.shape.setTransform(80.875,-2.725);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AApCCIgMhEIg3AAIgPBEIg1AAIBCkDIBBAAIA6EDgAAXAYIgUh4IgWB4IAqAAg");
	this.shape_1.setTransform(60.075,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ABECCIAAjqIgBAAIgyDqIgiAAIgxjqIAAAAIAADqIguAAIAAkDIBOAAIAiCoIAiioIBQAAIAAEDg");
	this.shape_2.setTransform(36.25,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAlCCIAAhiIABgzQABgVAEgRIhIC7Ig1AAIAAkDIAuAAIAABhIAAAgIgCAgIgEAbIBIi8IA1AAIAAEDg");
	this.shape_3.setTransform(12.025,0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AhQCCIAAkDIBdAAQAVAAAPAKQAPAKAIASQAJARAAAVQAAAWgJASQgIAQgQALQgPAKgVAAIgqAAIAABqgAgegNIAYAAQASgBAJgJQAJgKAAgUQAAgSgJgKQgJgJgSgBIgYAAg");
	this.shape_4.setTransform(-8.775,0.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgYCCIAAjcIg9AAIAAgnICrAAIAAAnIg9AAIAADcg");
	this.shape_5.setTransform(-28.875,0.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AA2CcIAAg0IhrAAIAAA0IgvAAIAAhcIAPAAQAHgOAEgOQAFgNACgQQADgOACgVIABguIAAhRICTAAIAADbIAPAAIAABcgAgNg4QAAAdgCAWQgDAUgFARQgFARgIAPIBIAAIAAi0IgxAAg");
	this.shape_6.setTransform(-50.325,3.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgYCCIAAkDIAxAAIAAEDg");
	this.shape_7.setTransform(-66.825,0.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAhCCIAAjcIhAAAIAADcIgyAAIAAkDICjAAIAAEDg");
	this.shape_8.setTransform(-82.1,0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-94.1,-22.6,188.3,45.2);


(lib.Tween19 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.techiia_logo();
	this.instance.setTransform(-63,-18,0.4865,0.4866);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-63,-18,243.3,36);


(lib.Tween15 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape.setTransform(132.725,16.775);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgNBIIAAh5IgiAAIAAgWIBfAAIAAAWIgiAAIAAB5g");
	this.shape_1.setTransform(124.225,16.775);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgbAAIAAiPIAbAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_2.setTransform(112.55,16.775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_3.setTransform(100.425,16.825);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgsBIIAAiPIAzAAQALAAAJAGQAIAFAFAKQAFAJAAAMQAAAMgFAKQgEAJgKAGQgIAFgMAAIgXAAIAAA7gAgRgHIAOAAQAJAAAGgGQAFgFAAgLQAAgKgFgFQgGgGgJAAIgOAAg");
	this.shape_4.setTransform(88.45,16.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgNBIIAAgSIgIAAQgMABgKgGQgLgGgHgMQgHgMAAgTQAAgSAHgNQAHgMALgGQAKgGAMAAIAIAAIAAgQIAbAAIAAAQIAIAAQALAAAMAGQAKAGAHAMQAHANAAASQAAATgHAMQgHAMgKAGQgLAGgMgBIgIAAIAAASgAAOAhIAGAAQAKAAAGgJQAFgIAAgQQAAgPgFgJQgGgJgKAAIgGAAgAgigYQgGAJAAAPQAAAQAGAIQAFAJAKAAIAGAAIAAhCIgGAAQgKAAgFAJg");
	this.shape_5.setTransform(74.35,16.775);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgYBJIgIgBIAAgWIADACIAFAAQAEAAADgDQADgCACgHIgohxIAfAAIAVBLIAXhLIAeAAIgoB0QgFARgHAHQgGAHgKAAIgJgBg");
	this.shape_6.setTransform(54.625,16.95);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAlBIIAAiCIgbCCIgTAAIgbiBIAACBIgaAAIAAiPIAsAAIASBdIAThdIAsAAIAACPg");
	this.shape_7.setTransform(40.9,16.775);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_8.setTransform(27.025,16.825);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_9.setTransform(14.9,16.775);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAUBcIAAg3IABgcQABgLACgKIAAAAIgoBoIgdAAIAAiQIAaAAIAAA2IAAASIgCASIgCAOIAAAAIAohoIAdAAIAACQgAgShBQgGgEgDgHQgBgHAAgIIANAAQAAAIAFAEQADAFAHAAQAGAAADgFQAEgEABgIIANAAQAAAIgBAHQgEAHgFAEQgHAEgKAAQgMAAgGgEg");
	this.shape_10.setTransform(2.75,14.825);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape_11.setTransform(-6.225,16.775);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAYBXIAAgdIhJAAIAAiQIAcAAIAAB6IAjAAIAAh6IAbAAIAAB6IAJAAIAAAzg");
	this.shape_12.setTransform(-15.15,18.225);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAXBIIgHglIgeAAIgIAlIgeAAIAliPIAkAAIAgCPgAANANIgLhCIgMBCIAXAAg");
	this.shape_13.setTransform(-27.575,16.775);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAlBIIAAiCIgbCCIgTAAIgciBIAACBIgZAAIAAiPIAsAAIASBdIAThdIAsAAIAACPg");
	this.shape_14.setTransform(-41.25,16.775);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgsBIIAAiPIAzAAQAMAAAIAGQAJAFAEAKQAEAJABAMQgBAMgEAKQgEAJgJAGQgJAFgLAAIgYAAIAAA7gAgRgHIAOAAQAKAAAEgGQAGgFAAgLQAAgKgGgFQgEgGgKAAIgOAAg");
	this.shape_15.setTransform(-54.95,16.775);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_16.setTransform(-67.025,16.825);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgMBIIAAgSIgIAAQgNABgLgGQgKgGgHgMQgHgMAAgTQAAgSAHgNQAHgMALgGQALgGAMAAIAIAAIAAgQIAaAAIAAAQIAIAAQALAAALAGQALAGAHAMQAHANAAASQAAATgHAMQgHAMgLAGQgKAGgMgBIgIAAIAAASgAAOAhIAGAAQALAAAEgJQAGgIAAgQQAAgPgGgJQgFgJgKAAIgGAAgAgjgYQgFAJAAAPQAAAQAFAIQAGAJAKAAIAHAAIAAhCIgHAAQgKAAgGAJg");
	this.shape_17.setTransform(-81.2,16.775);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_18.setTransform(-95.25,16.775);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape_19.setTransform(-104.175,16.775);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAXBIIgHglIgeAAIgIAlIgeAAIAliPIAkAAIAgCPgAANANIgLhCIgMBCIAXAAg");
	this.shape_20.setTransform(-118.775,16.775);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_21.setTransform(-130.7,16.775);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AggBjQgQgHgIgOQgJgPAAgaIAAhIQAAgaAJgPQAIgPAQgHQAOgHATAAQATAAAOAHQAPAGAIAQQAJAPAAAaIAABIQAAAagJAPQgIAOgPAHQgOAGgTABQgTgBgOgGgAgThAQgGAIAAARIAABRQAAAQAGAIQAHAJANgBQAMAAAHgHQAGgJAAgQIAAhRQAAgQgGgJQgHgKgMABQgNgBgHAKg");
	this.shape_22.setTransform(-34.55,-11.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AA0BlIAAi2IgmC2IgbAAIgmi2IAAC2IgkAAIAAjJIA9AAIAaCCIAbiCIA9AAIAADJg");
	this.shape_23.setTransform(-53.575,-11.15);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgtBZQgQgRAAgjIAAhVQAAgQAHgNQAHgNAOgHQANgIAUAAQAVAAANAIQAOAHAHAMQAHANAAARIAAAJIgmAAQAAgRgFgIQgFgKgOABQgMgBgFAKQgFAIABARIAAAQIAfAAIAAAeIgfAAIAAAkQAAAOAFAJQAGAJAKgBQAOAAAFgHQAFgJAAgPIAAgOIAmAAIAAALQAAAjgPAPQgPAQggAAQgdAAgQgRg");
	this.shape_24.setTransform(-72.225,-11.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AACBjQgNgHgJgOQgJgPAAgaIAAgeIgiAAIAABdIgnAAIAAjIIAnAAIAABNIAiAAIAAgOQAAgZAJgPQAJgPANgGQAPgHAUAAQATAAAPAHQAPAGAJAPQAIAPAAAZIAABJQAAAagIAPQgJAPgPAHQgPAGgTABQgUgBgPgGgAARhBQgIAJAAAQIAABQQAAAQAIAJQAGAKAOgBQANABAGgKQAIgJAAgQIAAhQQAAgQgIgJQgGgIgNAAQgOAAgGAIg");
	this.shape_25.setTransform(-92.2,-11.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AggBjQgQgHgIgOQgIgPgBgaIAAhIQABgaAIgPQAIgPAQgHQAOgHASAAQAUAAAOAHQAPAGAIAQQAJAPAAAaIAABIQAAAagJAPQgIAOgPAHQgOAGgUABQgSgBgOgGgAgThAQgGAIAAARIAABRQAAAQAGAIQAHAJAMgBQANAAAGgHQAHgJAAgQIAAhRQAAgQgHgJQgGgKgNABQgMgBgHAKg");
	this.shape_26.setTransform(-112.7,-11.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("Ag9BlIAAjJIBGAAQASAAALAHQALAGAFAMQAFALAAANQAAAKgDAJQgEAKgGAHQgGAGgIADQAPAFAHALQAIALAAAUQAAASgFANQgFAOgLAHQgKAIgPAAgAgWBIIAXAAQALABAFgIQAGgIAAgRQAAgOgGgIQgFgIgLAAIgXAAgAgWgSIAUAAQALgBAGgGQAGgIAAgNQAAgNgGgGQgFgHgMABIgUAAg");
	this.shape_27.setTransform(-128.675,-11.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-138.2,-29.8,276.5,59.6);


(lib.Tween14 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape.setTransform(132.725,16.775);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgNBIIAAh5IgiAAIAAgWIBfAAIAAAWIgiAAIAAB5g");
	this.shape_1.setTransform(124.225,16.775);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgbAAIAAiPIAbAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_2.setTransform(112.55,16.775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_3.setTransform(100.425,16.825);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgsBIIAAiPIAzAAQALAAAJAGQAIAFAFAKQAFAJAAAMQAAAMgFAKQgEAJgKAGQgIAFgMAAIgXAAIAAA7gAgRgHIAOAAQAJAAAGgGQAFgFAAgLQAAgKgFgFQgGgGgJAAIgOAAg");
	this.shape_4.setTransform(88.45,16.775);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgNBIIAAgSIgIAAQgMABgKgGQgLgGgHgMQgHgMAAgTQAAgSAHgNQAHgMALgGQAKgGAMAAIAIAAIAAgQIAbAAIAAAQIAIAAQALAAAMAGQAKAGAHAMQAHANAAASQAAATgHAMQgHAMgKAGQgLAGgMgBIgIAAIAAASgAAOAhIAGAAQAKAAAGgJQAFgIAAgQQAAgPgFgJQgGgJgKAAIgGAAgAgigYQgGAJAAAPQAAAQAGAIQAFAJAKAAIAGAAIAAhCIgGAAQgKAAgFAJg");
	this.shape_5.setTransform(74.35,16.775);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgYBJIgIgBIAAgWIADACIAFAAQAEAAADgDQADgCACgHIgohxIAfAAIAVBLIAXhLIAeAAIgoB0QgFARgHAHQgGAHgKAAIgJgBg");
	this.shape_6.setTransform(54.625,16.95);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAlBIIAAiCIgbCCIgTAAIgbiBIAACBIgaAAIAAiPIAsAAIASBdIAThdIAsAAIAACPg");
	this.shape_7.setTransform(40.9,16.775);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_8.setTransform(27.025,16.825);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_9.setTransform(14.9,16.775);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAUBcIAAg3IABgcQABgLACgKIAAAAIgoBoIgdAAIAAiQIAaAAIAAA2IAAASIgCASIgCAOIAAAAIAohoIAdAAIAACQgAgShBQgGgEgDgHQgBgHAAgIIANAAQAAAIAFAEQADAFAHAAQAGAAADgFQAEgEABgIIANAAQAAAIgBAHQgEAHgFAEQgHAEgKAAQgMAAgGgEg");
	this.shape_10.setTransform(2.75,14.825);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape_11.setTransform(-6.225,16.775);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAYBXIAAgdIhJAAIAAiQIAcAAIAAB6IAjAAIAAh6IAbAAIAAB6IAJAAIAAAzg");
	this.shape_12.setTransform(-15.15,18.225);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAXBIIgHglIgeAAIgIAlIgeAAIAliPIAkAAIAgCPgAANANIgLhCIgMBCIAXAAg");
	this.shape_13.setTransform(-27.575,16.775);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAlBIIAAiCIgbCCIgTAAIgciBIAACBIgZAAIAAiPIAsAAIASBdIAThdIAsAAIAACPg");
	this.shape_14.setTransform(-41.25,16.775);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgsBIIAAiPIAzAAQAMAAAIAGQAJAFAEAKQAEAJABAMQgBAMgEAKQgEAJgJAGQgJAFgLAAIgYAAIAAA7gAgRgHIAOAAQAKAAAEgGQAGgFAAgLQAAgKgGgFQgEgGgKAAIgOAAg");
	this.shape_15.setTransform(-54.95,16.775);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgXBHQgLgFgGgLQgGgKAAgSIAAg0QAAgSAGgLQAGgLALgFQAKgEANAAQAOAAALAEQAKAFAGALQAGALAAASIAAA0QAAASgGALQgGAKgKAFQgLAEgOAAQgNAAgKgEgAgNguQgFAGAAAMIAAA6QAAALAFAGQAFAGAIAAQAJAAAFgGQAFgGAAgLIAAg6QAAgLgFgHQgFgGgJAAQgIAAgFAGg");
	this.shape_16.setTransform(-67.025,16.825);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgMBIIAAgSIgIAAQgNABgLgGQgKgGgHgMQgHgMAAgTQAAgSAHgNQAHgMALgGQALgGAMAAIAIAAIAAgQIAaAAIAAAQIAIAAQALAAALAGQALAGAHAMQAHANAAASQAAATgHAMQgHAMgLAGQgKAGgMgBIgIAAIAAASgAAOAhIAGAAQALAAAEgJQAGgIAAgQQAAgPgGgJQgFgJgKAAIgGAAgAgjgYQgFAJAAAPQAAAQAFAIQAGAJAKAAIAHAAIAAhCIgHAAQgKAAgGAJg");
	this.shape_17.setTransform(-81.2,16.775);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_18.setTransform(-95.25,16.775);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgNBIIAAiPIAbAAIAACPg");
	this.shape_19.setTransform(-104.175,16.775);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAXBIIgHglIgeAAIgIAlIgeAAIAliPIAkAAIAgCPgAANANIgLhCIgMBCIAXAAg");
	this.shape_20.setTransform(-118.775,16.775);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AASBIIAAhDIgjAAIAABDIgcAAIAAiPIAcAAIAAA3IAjAAIAAg3IAbAAIAACPg");
	this.shape_21.setTransform(-130.7,16.775);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AggBjQgQgHgIgOQgJgPAAgaIAAhIQAAgaAJgPQAIgPAQgHQAOgHATAAQATAAAOAHQAPAGAIAQQAJAPAAAaIAABIQAAAagJAPQgIAOgPAHQgOAGgTABQgTgBgOgGgAgThAQgGAIAAARIAABRQAAAQAGAIQAHAJANgBQAMAAAHgHQAGgJAAgQIAAhRQAAgQgGgJQgHgKgMABQgNgBgHAKg");
	this.shape_22.setTransform(-34.55,-11.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AA0BlIAAi2IgmC2IgbAAIgmi2IAAC2IgkAAIAAjJIA9AAIAaCCIAbiCIA9AAIAADJg");
	this.shape_23.setTransform(-53.575,-11.15);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgtBZQgQgRAAgjIAAhVQAAgQAHgNQAHgNAOgHQANgIAUAAQAVAAANAIQAOAHAHAMQAHANAAARIAAAJIgmAAQAAgRgFgIQgFgKgOABQgMgBgFAKQgFAIABARIAAAQIAfAAIAAAeIgfAAIAAAkQAAAOAFAJQAGAJAKgBQAOAAAFgHQAFgJAAgPIAAgOIAmAAIAAALQAAAjgPAPQgPAQggAAQgdAAgQgRg");
	this.shape_24.setTransform(-72.225,-11.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AACBjQgNgHgJgOQgJgPAAgaIAAgeIgiAAIAABdIgnAAIAAjIIAnAAIAABNIAiAAIAAgOQAAgZAJgPQAJgPANgGQAPgHAUAAQATAAAPAHQAPAGAJAPQAIAPAAAZIAABJQAAAagIAPQgJAPgPAHQgPAGgTABQgUgBgPgGgAARhBQgIAJAAAQIAABQQAAAQAIAJQAGAKAOgBQANABAGgKQAIgJAAgQIAAhQQAAgQgIgJQgGgIgNAAQgOAAgGAIg");
	this.shape_25.setTransform(-92.2,-11.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AggBjQgQgHgIgOQgIgPgBgaIAAhIQABgaAIgPQAIgPAQgHQAOgHASAAQAUAAAOAHQAPAGAIAQQAJAPAAAaIAABIQAAAagJAPQgIAOgPAHQgOAGgUABQgSgBgOgGgAgThAQgGAIAAARIAABRQAAAQAGAIQAHAJAMgBQANAAAGgHQAHgJAAgQIAAhRQAAgQgHgJQgGgKgNABQgMgBgHAKg");
	this.shape_26.setTransform(-112.7,-11.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("Ag9BlIAAjJIBGAAQASAAALAHQALAGAFAMQAFALAAANQAAAKgDAJQgEAKgGAHQgGAGgIADQAPAFAHALQAIALAAAUQAAASgFANQgFAOgLAHQgKAIgPAAgAgWBIIAXAAQALABAFgIQAGgIAAgRQAAgOgGgIQgFgIgLAAIgXAAgAgWgSIAUAAQALgBAGgGQAGgIAAgNQAAgNgGgGQgFgHgMABIgUAAg");
	this.shape_27.setTransform(-128.675,-11.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-138.2,-29.8,276.5,59.6);


(lib.Tween13 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAlClIAAhiIABg0QABgUAEgRIhIC7Ig1AAIAAkDIAuAAIAABhIAAAhIgCAgIgEAaIBIi8IA1AAIAAEDgAghh2QgLgIgEgMQgEgMAAgOIAZAAQAAAOAHAIQAHAHAMAAQALAAAHgHQAHgIAAgOIAaAAQAAAOgEAMQgFAMgLAIQgMAIgTAAQgVAAgLgIg");
	this.shape.setTransform(80.875,-2.725);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AApCCIgMhEIg3AAIgPBEIg1AAIBCkDIBBAAIA6EDgAAXAYIgUh4IgWB4IAqAAg");
	this.shape_1.setTransform(60.075,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ABECCIAAjqIgBAAIgyDqIgiAAIgxjqIAAAAIAADqIguAAIAAkDIBOAAIAiCoIAiioIBQAAIAAEDg");
	this.shape_2.setTransform(36.25,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAlCCIAAhiIABgzQABgVAEgRIhIC7Ig1AAIAAkDIAuAAIAABhIAAAgIgCAgIgEAbIBIi8IA1AAIAAEDg");
	this.shape_3.setTransform(12.025,0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AhQCCIAAkDIBdAAQAVAAAPAKQAPAKAIASQAJARAAAVQAAAWgJASQgIAQgQALQgPAKgVAAIgqAAIAABqgAgegNIAYAAQASgBAJgJQAJgKAAgUQAAgSgJgKQgJgJgSgBIgYAAg");
	this.shape_4.setTransform(-8.775,0.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgYCCIAAjcIg9AAIAAgnICrAAIAAAnIg9AAIAADcg");
	this.shape_5.setTransform(-28.875,0.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AA2CcIAAg0IhrAAIAAA0IgvAAIAAhcIAPAAQAHgOAEgOQAFgNACgQQADgOACgVIABguIAAhRICTAAIAADbIAPAAIAABcgAgNg4QAAAdgCAWQgDAUgFARQgFARgIAPIBIAAIAAi0IgxAAg");
	this.shape_6.setTransform(-50.325,3.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgYCCIAAkDIAxAAIAAEDg");
	this.shape_7.setTransform(-66.825,0.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAhCCIAAjcIhAAAIAADcIgyAAIAAkDICjAAIAAEDg");
	this.shape_8.setTransform(-82.1,0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-94.1,-22.6,188.3,45.2);


(lib.Tween12 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAlClIAAhiIABg0QABgUAEgRIhIC7Ig1AAIAAkDIAuAAIAABhIAAAhIgCAgIgEAaIBIi8IA1AAIAAEDgAghh2QgLgIgEgMQgEgMAAgOIAZAAQAAAOAHAIQAHAHAMAAQALAAAHgHQAHgIAAgOIAaAAQAAAOgEAMQgFAMgLAIQgMAIgTAAQgVAAgLgIg");
	this.shape.setTransform(80.875,-2.725);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AApCCIgMhEIg3AAIgPBEIg1AAIBCkDIBBAAIA6EDgAAXAYIgUh4IgWB4IAqAAg");
	this.shape_1.setTransform(60.075,0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("ABECCIAAjqIgBAAIgyDqIgiAAIgxjqIAAAAIAADqIguAAIAAkDIBOAAIAiCoIAiioIBQAAIAAEDg");
	this.shape_2.setTransform(36.25,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAlCCIAAhiIABgzQABgVAEgRIhIC7Ig1AAIAAkDIAuAAIAABhIAAAgIgCAgIgEAbIBIi8IA1AAIAAEDg");
	this.shape_3.setTransform(12.025,0.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AhQCCIAAkDIBdAAQAVAAAPAKQAPAKAIASQAJARAAAVQAAAWgJASQgIAQgQALQgPAKgVAAIgqAAIAABqgAgegNIAYAAQASgBAJgJQAJgKAAgUQAAgSgJgKQgJgJgSgBIgYAAg");
	this.shape_4.setTransform(-8.775,0.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgYCCIAAjcIg9AAIAAgnICrAAIAAAnIg9AAIAADcg");
	this.shape_5.setTransform(-28.875,0.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AA2CcIAAg0IhrAAIAAA0IgvAAIAAhcIAPAAQAHgOAEgOQAFgNACgQQADgOACgVIABguIAAhRICTAAIAADbIAPAAIAABcgAgNg4QAAAdgCAWQgDAUgFARQgFARgIAPIBIAAIAAi0IgxAAg");
	this.shape_6.setTransform(-50.325,3.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgYCCIAAkDIAxAAIAAEDg");
	this.shape_7.setTransform(-66.825,0.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAhCCIAAjcIhAAAIAADcIgyAAIAAkDICjAAIAAEDg");
	this.shape_8.setTransform(-82.1,0.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-94.1,-22.6,188.3,45.2);


(lib.Tween11 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AggBcQgOgGgJgNQgIgNAAgVIAAhNQAAgVAIgNQAJgNAOgGQAOgGASAAQATAAAOAGQAOAGAJANQAIANAAAVIAAAqIhVAAIAAAoQAAALAGAFQAGAFAJAAQAKAAAGgFQAGgGAAgKIAAgKIAqAAIAAAFQAAAVgIANQgJANgOAGQgOAGgTAAQgSAAgOgGgAgPg6QgGAEAAAKIAAASIArAAIAAgSQAAgKgGgEQgGgGgKAAQgJAAgGAGg");
	this.shape.setTransform(52.475,3.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKBvQgLgJAAgSIAAh3IgRAAIAAghIARAAIAAgyIApAAIAAAyIATAAIAAAhIgTAAIAABmQAAALADAEQAEAEAMAAIAAAgIgJABIgLAAQgTAAgKgIg");
	this.shape_1.setTransform(34.925,1.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag1BWQgNgNgBgbQAAgQAGgLQAEgLALgIQALgIAPgGIATgIQAIgFAGgGQAFgGABgHQAAgJgGgDQgFgFgJAAQgJABgGAFQgGAGAAAMIAAAEIgpAAIAAgJQAAgPAIgMQAHgMAPgHQAOgHASAAQARAAAOAFQAPAFAJALQAIAKABARIAAByQgBAJACAHQABAGACAFIgrAAIgDgIIAAgJQgHALgJAFQgHAHgRAAQgVAAgNgMgAgIAPQgIAEgEAIQgDAIABAGQgBALAEAHQAFAHAKABQAJgBAHgFQAGgHABgMIAAgsg");
	this.shape_2.setTransform(17.65,3.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAUBfIAAiEQAAgLgFgGQgGgGgJAAQgJAAgFAGQgFAGAAALIAACEIgqAAIAAi4IAqAAIAAAPQAFgHAIgHQAJgGANAAQANAAAKAFQALAFAFAMQAHAMAAASIAACJg");
	this.shape_3.setTransform(-3.55,3.475);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmBZQgPgIgGgNQgGgOAAgRIAAhKQAAgQAGgNQAGgOAPgJQAOgHAYgBQAYABAPAHQAOAJAHAOQAGANAAAQIAABKQAAARgGAOQgHANgOAIQgPAJgYAAQgYAAgOgJgAgPg7QgGAEAAANIAABVQAAAMAGAFQAGAFAJAAQALAAAFgEQAGgFAAgNIAAhVQAAgMgGgGQgGgEgKAAQgJAAgGAFg");
	this.shape_4.setTransform(-24.5268,3.75);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgmB4QgKgGgIgLQgGgMAAgSIAAhaQAAgTAGgMQAIgLAKgGQAMgEANAAQAMAAAIAFQAKAGAEAHIAAhJIAqAAIAAD0IgqAAIAAgTQgFALgKAHQgJAGgKABQgNAAgMgGgAgMgeQgHAGAAALIAABTQAAALAHAGQAGAFAHABQAJgBAFgFQAGgGAAgLIAAhUQAAgKgGgGQgFgFgJAAQgHAAgGAFg");
	this.shape_5.setTransform(-45.65,1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#050581","#0000BE"],[0,1],-135,0,135,0).s().p("AxOD2QhnAAhIhIQhIhIAAhmQAAhlBIhJQBIhIBnAAMAieAAAQBlAABIBIQBJBJAABlQAABmhJBIQhIBIhlAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-135,-24.6,270,49.3);


(lib.Tween10 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AggBcQgOgGgJgNQgIgNAAgVIAAhNQAAgVAIgNQAJgNAOgGQAOgGASAAQATAAAOAGQAOAGAJANQAIANAAAVIAAAqIhVAAIAAAoQAAALAGAFQAGAFAJAAQAKAAAGgFQAGgGAAgKIAAgKIAqAAIAAAFQAAAVgIANQgJANgOAGQgOAGgTAAQgSAAgOgGgAgPg6QgGAEAAAKIAAASIArAAIAAgSQAAgKgGgEQgGgGgKAAQgJAAgGAGg");
	this.shape.setTransform(52.475,3.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKBvQgLgJAAgSIAAh3IgRAAIAAghIARAAIAAgyIApAAIAAAyIATAAIAAAhIgTAAIAABmQAAALADAEQAEAEAMAAIAAAgIgJABIgLAAQgTAAgKgIg");
	this.shape_1.setTransform(34.925,1.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag1BWQgNgNgBgbQAAgQAGgLQAEgLALgIQALgIAPgGIATgIQAIgFAGgGQAFgGABgHQAAgJgGgDQgFgFgJAAQgJABgGAFQgGAGAAAMIAAAEIgpAAIAAgJQAAgPAIgMQAHgMAPgHQAOgHASAAQARAAAOAFQAPAFAJALQAIAKABARIAAByQgBAJACAHQABAGACAFIgrAAIgDgIIAAgJQgHALgJAFQgHAHgRAAQgVAAgNgMgAgIAPQgIAEgEAIQgDAIABAGQgBALAEAHQAFAHAKABQAJgBAHgFQAGgHABgMIAAgsg");
	this.shape_2.setTransform(17.65,3.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAUBfIAAiEQAAgLgFgGQgGgGgJAAQgJAAgFAGQgFAGAAALIAACEIgqAAIAAi4IAqAAIAAAPQAFgHAIgHQAJgGANAAQANAAAKAFQALAFAFAMQAHAMAAASIAACJg");
	this.shape_3.setTransform(-3.55,3.475);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmBZQgPgIgGgNQgGgOAAgRIAAhKQAAgQAGgNQAGgOAPgJQAOgHAYgBQAYABAPAHQAOAJAHAOQAGANAAAQIAABKQAAARgGAOQgHANgOAIQgPAJgYAAQgYAAgOgJgAgPg7QgGAEAAANIAABVQAAAMAGAFQAGAFAJAAQALAAAFgEQAGgFAAgNIAAhVQAAgMgGgGQgGgEgKAAQgJAAgGAFg");
	this.shape_4.setTransform(-24.5268,3.75);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgmB4QgKgGgIgLQgGgMAAgSIAAhaQAAgTAGgMQAIgLAKgGQAMgEANAAQAMAAAIAFQAKAGAEAHIAAhJIAqAAIAAD0IgqAAIAAgTQgFALgKAHQgJAGgKABQgNAAgMgGgAgMgeQgHAGAAALIAABTQAAALAHAGQAGAFAHABQAJgBAFgFQAGgGAAgLIAAhUQAAgKgGgGQgFgFgJAAQgHAAgGAFg");
	this.shape_5.setTransform(-45.65,1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#050581","#0000BE"],[0,1],-135,0,135,0).s().p("AxOD2QhnAAhIhIQhIhIAAhmQAAhlBIhJQBIhIBnAAMAieAAAQBlAABIBIQBJBJAABlQAABmhJBIQhIBIhlAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-135,-24.6,270,49.3);


(lib.Tween9 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AggBcQgOgGgJgNQgIgNAAgVIAAhNQAAgVAIgNQAJgNAOgGQAOgGASAAQATAAAOAGQAOAGAJANQAIANAAAVIAAAqIhVAAIAAAoQAAALAGAFQAGAFAJAAQAKAAAGgFQAGgGAAgKIAAgKIAqAAIAAAFQAAAVgIANQgJANgOAGQgOAGgTAAQgSAAgOgGgAgPg6QgGAEAAAKIAAASIArAAIAAgSQAAgKgGgEQgGgGgKAAQgJAAgGAGg");
	this.shape.setTransform(52.475,3.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKBvQgLgJAAgSIAAh3IgRAAIAAghIARAAIAAgyIApAAIAAAyIATAAIAAAhIgTAAIAABmQAAALADAEQAEAEAMAAIAAAgIgJABIgLAAQgTAAgKgIg");
	this.shape_1.setTransform(34.925,1.35);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag1BWQgNgNgBgbQAAgQAGgLQAEgLALgIQALgIAPgGIATgIQAIgFAGgGQAFgGABgHQAAgJgGgDQgFgFgJAAQgJABgGAFQgGAGAAAMIAAAEIgpAAIAAgJQAAgPAIgMQAHgMAPgHQAOgHASAAQARAAAOAFQAPAFAJALQAIAKABARIAAByQgBAJACAHQABAGACAFIgrAAIgDgIIAAgJQgHALgJAFQgHAHgRAAQgVAAgNgMgAgIAPQgIAEgEAIQgDAIABAGQgBALAEAHQAFAHAKABQAJgBAHgFQAGgHABgMIAAgsg");
	this.shape_2.setTransform(17.65,3.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAUBfIAAiEQAAgLgFgGQgGgGgJAAQgJAAgFAGQgFAGAAALIAACEIgqAAIAAi4IAqAAIAAAPQAFgHAIgHQAJgGANAAQANAAAKAFQALAFAFAMQAHAMAAASIAACJg");
	this.shape_3.setTransform(-3.55,3.475);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgmBZQgPgIgGgNQgGgOAAgRIAAhKQAAgQAGgNQAGgOAPgJQAOgHAYgBQAYABAPAHQAOAJAHAOQAGANAAAQIAABKQAAARgGAOQgHANgOAIQgPAJgYAAQgYAAgOgJgAgPg7QgGAEAAANIAABVQAAAMAGAFQAGAFAJAAQALAAAFgEQAGgFAAgNIAAhVQAAgMgGgGQgGgEgKAAQgJAAgGAFg");
	this.shape_4.setTransform(-24.5268,3.75);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgmB4QgKgGgIgLQgGgMAAgSIAAhaQAAgTAGgMQAIgLAKgGQAMgEANAAQAMAAAIAFQAKAGAEAHIAAhJIAqAAIAAD0IgqAAIAAgTQgFALgKAHQgJAGgKABQgNAAgMgGgAgMgeQgHAGAAALIAABTQAAALAHAGQAGAFAHABQAJgBAFgFQAGgGAAgLIAAhUQAAgKgGgGQgFgFgJAAQgHAAgGAFg");
	this.shape_5.setTransform(-45.65,1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#050581","#0000BE"],[0,1],-135,0,135,0).s().p("AxOD2QhnAAhIhIQhIhIAAhmQAAhlBIhJQBIhIBnAAMAieAAAQBlAABIBIQBJBJAABlQAABmhJBIQhIBIhlAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-135,-24.6,270,49.3);


(lib.Tween8 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.delo();
	this.instance.setTransform(-55.7,-28,0.5384,0.5385);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55.7,-28,111.5,56);


(lib.Tween7 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.delo();
	this.instance.setTransform(-55.7,-28,0.5384,0.5385);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55.7,-28,111.5,56);


(lib.Tween5 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Layer_1
	this.instance = new lib.top_sto();
	this.instance.setTransform(-40.65,-28,0.4517,0.4516);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40.6,-28,81.30000000000001,56);


// stage content:
(lib.desctop_partners_v1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	this.actionFrames = [0];
	// timeline functions:
	this.frame_0 = function() {
		admixAPI.on("load", function(){
            admixAPI.init({
                resize: [{"name":"state-1","width":1200,"height":60}]
            });
            function $(id){
			return document.getElementById(id);
			}

			document.body.onclick = function(){
				admixAPI.click('');
			};
			document.body.onselectstart = function() {
				return false;
			}
        });
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(157));

	// button
	this.instance = new lib.Tween26("synched",0);
	this.instance.setTransform(1045.15,90);
	this.instance.alpha = 0;

	this.instance_1 = new lib.Tween27("synched",0);
	this.instance_1.setTransform(1045.15,30);

	this.instance_2 = new lib.Tween9("synched",0);
	this.instance_2.setTransform(1045.15,30);
	this.instance_2._off = true;

	this.instance_3 = new lib.Tween10("synched",0);
	this.instance_3.setTransform(1045.15,29.95,1.1,1.1);
	this.instance_3._off = true;

	this.instance_4 = new lib.Tween11("synched",0);
	this.instance_4.setTransform(1045.15,30);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},4).to({state:[{t:this.instance_2}]},12).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_2}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_2}]},24).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_2}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_4}]},10).to({state:[{t:this.instance_4}]},4).to({state:[]},1).wait(60));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,y:30,alpha:1},4).wait(153));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(16).to({_off:false},0).to({_off:true,scaleX:1.1,scaleY:1.1,y:29.95},3).wait(9).to({_off:false,scaleX:1,scaleY:1,y:30},0).to({_off:true,scaleX:1.1,scaleY:1.1,y:29.95},3).wait(30).to({_off:false,scaleX:1,scaleY:1,y:30},0).to({_off:true,scaleX:1.1,scaleY:1.1,y:29.95},3).wait(9).to({_off:false,scaleX:1,scaleY:1,y:30},0).to({_off:true,scaleX:1.1,scaleY:1.1,y:29.95},3).wait(81));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(16).to({_off:false},3).wait(3).to({startPosition:0},0).to({_off:true,scaleX:1,scaleY:1,y:30},3).wait(3).to({_off:false,scaleX:1.1,scaleY:1.1,y:29.95},3).wait(3).to({startPosition:0},0).to({_off:true,scaleX:1,scaleY:1,y:30},3).wait(24).to({_off:false,scaleX:1.1,scaleY:1.1,y:29.95},3).wait(3).to({startPosition:0},0).to({_off:true,scaleX:1,scaleY:1,y:30},3).wait(3).to({_off:false,scaleX:1.1,scaleY:1.1,y:29.95},3).wait(3).to({startPosition:0},0).to({_off:true,scaleX:1,scaleY:1,y:30},3).wait(75));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(22).to({_off:false},3).to({_off:true},3).wait(6).to({_off:false},3).to({_off:true},24).wait(6).to({_off:false},3).to({_off:true},3).wait(6).to({_off:false},3).wait(10).to({startPosition:0},0).to({y:-40},4).to({_off:true},1).wait(60));

	// logo_title_new
	this.instance_5 = new lib.Tween28("synched",0);
	this.instance_5.setTransform(318,60.05);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.instance_6 = new lib.Tween29("synched",0);
	this.instance_6.setTransform(318,32);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(94).to({_off:false},0).to({_off:true,y:32,alpha:1},4).wait(59));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(94).to({_off:false},4).wait(54).to({startPosition:0},0).to({y:-20,alpha:0},4).wait(1));

	// logos
	this.instance_7 = new lib.Tween19("synched",0);
	this.instance_7.setTransform(700.2,80.6,1.0001,0.9999,0,0,0,0.5,0.6);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(94).to({_off:false},0).to({y:30.6,alpha:1},4).wait(54).to({startPosition:0},0).to({y:-19.4,alpha:0},4).wait(1));

	// delo
	this.instance_8 = new lib.Tween24("synched",0);
	this.instance_8.setTransform(794.7,92);
	this.instance_8.alpha = 0;

	this.instance_9 = new lib.Tween25("synched",0);
	this.instance_9.setTransform(794.7,30);

	this.instance_10 = new lib.Tween7("synched",0);
	this.instance_10.setTransform(794.7,30);
	this.instance_10._off = true;

	this.instance_11 = new lib.Tween8("synched",0);
	this.instance_11.setTransform(794.7,-32.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_8}]}).to({state:[{t:this.instance_9}]},4).to({state:[{t:this.instance_10}]},43).to({state:[{t:this.instance_11}]},4).to({state:[]},1).wait(105));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({_off:true,y:30,alpha:1},4).wait(153));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(47).to({_off:false},0).to({_off:true,y:-32.5},4).wait(106));

	// top_sto
	this.instance_12 = new lib.Tween5("synched",0);
	this.instance_12.setTransform(779.65,92.5);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(48).to({_off:false},0).to({y:30},4).wait(40).to({startPosition:0},0).to({y:-32.5},4).to({_off:true},1).wait(60));

	// text_2
	this.instance_13 = new lib.Tween20("synched",0);
	this.instance_13.setTransform(631.75,92);
	this.instance_13.alpha = 0;

	this.instance_14 = new lib.Tween21("synched",0);
	this.instance_14.setTransform(631.75,30);

	this.instance_15 = new lib.Tween12("synched",0);
	this.instance_15.setTransform(631.75,30);
	this.instance_15._off = true;

	this.instance_16 = new lib.Tween13("synched",0);
	this.instance_16.setTransform(631.75,-23.95);
	this.instance_16.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_13}]}).to({state:[{t:this.instance_14}]},4).to({state:[{t:this.instance_15}]},88).to({state:[{t:this.instance_16}]},4).to({state:[]},1).wait(60));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({_off:true,y:30,alpha:1},4).wait(153));
	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(92).to({_off:false},0).to({_off:true,y:-23.95,alpha:0},4).wait(61));

	// text_1
	this.instance_17 = new lib.Tween22("synched",0);
	this.instance_17.setTransform(346.15,92);
	this.instance_17.alpha = 0;

	this.instance_18 = new lib.Tween23("synched",0);
	this.instance_18.setTransform(346.15,28.45);

	this.instance_19 = new lib.Tween14("synched",0);
	this.instance_19.setTransform(346.15,28.45);
	this.instance_19._off = true;

	this.instance_20 = new lib.Tween15("synched",0);
	this.instance_20.setTransform(346.15,-31.55);
	this.instance_20.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_17}]}).to({state:[{t:this.instance_18}]},4).to({state:[{t:this.instance_19}]},88).to({state:[{t:this.instance_20}]},4).to({state:[]},1).wait(60));
	this.timeline.addTween(cjs.Tween.get(this.instance_17).to({_off:true,y:28.45,alpha:1},4).wait(153));
	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(92).to({_off:false},0).to({_off:true,y:-31.55,alpha:0},4).wait(61));

	// prapor
	this.instance_21 = new lib.prapor();
	this.instance_21.setTransform(-84,-80,0.6719,0.672);

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(157));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(516,-50,677.7,171.8);
// library properties:
lib.properties = {
	id: 'A1E6028AF6A70B4B8BD703CC9DB3B936',
	width: 1200,
	height: 60,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/body_atlas_1.png", id:"body_atlas_1"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['A1E6028AF6A70B4B8BD703CC9DB3B936'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused || stageChild.ignorePause){
			stageChild.syncStreamSounds();
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;