try {
    (function(w, d, f) {
        'use strict';

        var SimpleCollectorData = {
            UserAgentData: function() {
                try {
                    return w.navigator.userAgent;
                } catch (e) { return 'Undefined'; }
            },

            LanguageData: function() {
                try {
                    var out =  w.navigator.languages;
                    if (Array.isArray(out))
                        out = out.join("\t");
                    return out;
                } catch (e) { return 'Undefined'; }
            },

            WebdriverData: function() {
                try {
                    return Number(!!w.navigator.webdriver).toString();
                } catch (e) { return 'Undefined'; }
            },

            ChromeData: function() {
                try {
                    return Number(typeof w.chrome !== 'undefined').toString();
                } catch (e) { return 'Undefined'; }
            },

            PluginsData: function() {
                try {
                    var out = Object.entries(w.navigator.plugins).
                        filter(function(v) {
                            return (v[1] instanceof Plugin);
                        }).
                        map(function(v, k) {
                            return v[1].name;
                        })
                    ;

                    if (Array.isArray(out))
                        out = out.join("\t");
                    return out;
                } catch (e) { return 'Undefined'; }
            },

            WebGlData: function() {
                try {
                    var canvas = document.createElement('canvas');
                    var gl = canvas.getContext('webgl');

                    var debugInfo = gl.getExtension('WEBGL_debug_renderer_info');
                    var vendor = gl.getParameter(debugInfo.UNMASKED_VENDOR_WEBGL);
                    var renderer = gl.getParameter(debugInfo.UNMASKED_RENDERER_WEBGL);

                    return [
                        vendor,
                        renderer
                    ].join("\t");
                } catch (e) { return 'Undefined'; }
            },

            TimezoneData: function() {
                try {
                    var param = w.Intl.DateTimeFormat().resolvedOptions().timeZone;
                    if (typeof param === 'undefined') {
                        return "not available";
                    } else {
                        return param;
                    }
                } catch (e) { return 'Undefined'; }
            },

            TimeoffsetData: function() {
                try {
                    var timeOffset = (new Date()).getTimezoneOffset();
                    if (timeOffset == undefined) {
                        return null;
                    } else {
                        return timeOffset.toString();
                    }
                } catch (e) { return 'Undefined'; }
            },

            BatteryData: function() {
                try {
                    return ((!!('getBattery' in navigator)) ? 1 : 0).toString();
                } catch (e) { return 'Undefined'; }
            },

            LiedBrowserData: function() {
                try {
                    return (hasLiedBrowser() ? 1 : 0).toString();
                } catch (e) { return 'Undefined'; }
            },

            HiddenFuncData: function() {
                try {
                    return getHiddenFunc();
                } catch (e) { return 'Undefined'; }
            },

            DeviceMemoryData: function() {
                try {
                    if (navigator.deviceMemory == undefined) {
                        return 'n/a';
                    } else {
                        return navigator.deviceMemory.toString();
                    }
                } catch (e) { return 'Undefined'; }
            },
            
            TrackData: function() {
                try {
                    if (typeof navigator.doNotTrack === 'undefined')
                        return '-1';
                    var dnt = navigator.doNotTrack.toString().toLowerCase();
                    return ['1', 'yes', 'on'].indexOf(dnt) !== -1 ? '1' : '0';
                } catch (e) { return 'Undefined'; }
            },

            NavTypeData: function() {
                try {
                    var n = performance.navigation.type;
                    return 0 === n ? "navigate" : 1 === n ? "reload" : 2 === n ? "back-forward" : n;
                } catch (e) { return 'Undefined'; }
            },

            ClipboardData: function() {
                try {
                    return (navigator.clipboard != null) && (navigator.clipboard != undefined)
                } catch (e) { return 'Undefined'; }
            },

            ScreenResAvailData: function() {
                try {
                    if (w.screen.availWidth && w.screen.availHeight)
                        return w.screen.availWidth +'x'+ w.screen.availHeight;
                    return false;
                } catch (e) { return 'Undefined'; }
            }
        };

        (function () {
            try {
                if (typeof window.fixidleDataC !== 'undefined')
                    return false;

                var post = 'data='+ JSON.stringify([
                    ['UserAgentData', SimpleCollectorData.UserAgentData()],
                    ['LanguageData', SimpleCollectorData.LanguageData()],
                    ['WebdriverData', SimpleCollectorData.WebdriverData()],
                    ['ChromeData', SimpleCollectorData.ChromeData()],
                    ['PluginsData', SimpleCollectorData.PluginsData()],
                    ['NavTypeData', SimpleCollectorData.NavTypeData()],
                    ['ClipboardData', SimpleCollectorData.ClipboardData()],
                    ['ScreenResAvailData', SimpleCollectorData.ScreenResAvailData()],
                    ['WebGlData', SimpleCollectorData.WebGlData()],
                    ['TimezoneData', SimpleCollectorData.TimezoneData()],
                    ['TimeoffsetData', SimpleCollectorData.TimeoffsetData()],
                    ['BatteryData', SimpleCollectorData.BatteryData()],
                    ['DeviceMemoryData', SimpleCollectorData.DeviceMemoryData()],
                    ['TrackData', SimpleCollectorData.TrackData()],
                ]);

                post += '&url='+ encodeURIComponent(d.location.href);
                post += '&referer='+ encodeURIComponent(d.referrer);
                post += '&rtuid='+ encodeURIComponent('cba3268b7f53d5fe40e657825389dc75');
                post += '&code='+ encodeURIComponent('news');

                var url = 'https://fixidle.com/?v='+ (new Date()).getTime() + (1*Math.random());

                var xhr = new XMLHttpRequest();
                xhr.open('POST', url, true);
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.onreadystatechange = function() {
                    if(xhr.readyState == XMLHttpRequest.DONE) {
                        if (xhr.status == 200) {
                            var ct = xhr.getResponseHeader('content-type'),
                                re = new RegExp('application/json')
                            ;

                            if (re.test(ct)) {
                                try {
                                    var data = JSON.parse(xhr.response);

                                    f.collection = (
                                        typeof data.collection !== 'undefined'
                                    ) ? data.collection : '';

                                    f.segment = (
                                        typeof data.segment !== 'undefined'
                                    ) ? data.segment : -1;
                                } catch (e) {}
                            }
                        } else {
                            var img = document.createElement("img");
                            img.setAttribute('src', 'https://fixidle.com/pixel.gif?t=xhr&s='+ xhr.status);
                            img.setAttribute('style', 'width: 0; height: 0; border: none;');
                            document.getElementsByTagName('body')[0].appendChild(img);
                            img.remove();
                        }
                    }
                };
                xhr.send(post);

                window.fixidleDataC = true;
            } catch (e) {
                var img = document.createElement("img");
                img.setAttribute('src', 'https://fixidle.com/uploads/pixel.gif?t=exc2');
                img.setAttribute('style', 'width: 0; height: 0; border: none;');
                document.getElementsByTagName('body')[0].appendChild(img);
                img.remove();
            };
        })();
    })(window, document, window.fixidleData || (window.fixidleData = {}));
} catch (e) {
    var img = document.createElement("img");
    img.setAttribute('src', 'https://fixidle.com/pixel.gif?t=exc');
    img.setAttribute('style', 'width: 0; height: 0; border: none;');
    document.getElementsByTagName('body')[0].appendChild(img);
    img.remove();
}
