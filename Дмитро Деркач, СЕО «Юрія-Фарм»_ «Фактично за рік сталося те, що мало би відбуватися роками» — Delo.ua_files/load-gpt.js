(function(){
    window.googletag = window.googletag || {cmd: []};
    window.googletag.cmd = window.googletag.cmd || [];
    var gptUrl = window.ADM_GPT_SRC || "https://securepubads.g.doubleclick.net/tag/js/gpt.js";
    googletag.cmd.push(function () {
		googletag.pubads().enableSingleRequest();
        googletag.pubads().disableInitialLoad();
		googletag.enableServices();
    });
    var gpt_script = document.createElement('script');
    gpt_script.async = true;
    gpt_script.src = gptUrl;
    document.head.appendChild(gpt_script);
})();
